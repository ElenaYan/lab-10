package it.unibo.oop.lab.lambda.ex02;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 */
public final class MusicGroupImpl implements MusicGroup {

	private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
    Stream<String> names = songs.stream()
    		                    .map(s -> s.getSongName())
    		                    .sorted();
    
        return names;
    }

    @Override
    public Stream<String> albumNames() {
        return albums.keySet().stream();
    }

    @Override
    public Stream<String> albumInYear(final int year) {
    Stream<String> album = albums.entrySet()
    							 .stream()
    							 .filter(s -> s.getValue() == year)
    							 .map(s -> s.getKey());
        return album;
    }

    @Override
    public int countSongs(final String albumName) {
    	Long numSongs = songs.stream()
    			             .filter(s -> s.getAlbumName().isPresent())
    			             .filter(s -> s.getAlbumName().get().equals(albumName))
    			             .count();
    	
    	return numSongs.intValue();
    }

    @Override
    public int countSongsInNoAlbum() {
    	Long numSongs = songs.stream()
    			             .filter(s -> s.getAlbumName()
    			             .isEmpty())
    			             .count();
    	
        return numSongs.intValue();
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
    	OptionalDouble ris = songs.stream()
    			                  .filter(s -> s.getAlbumName().isPresent())
    			                  .filter(s -> s.getAlbumName().get().equals(albumName))
    			                  .mapToDouble(s -> s.getDuration())
    			                  .average();
    	return ris;
    }

    @Override
    public Optional<String> longestSong() {
    	Optional<String> longest = Optional.of(songs.stream()
    			                                    .max((s1, s2) -> Double.compare(s1.getDuration(), s2.getDuration()))
    			                                    .get()
    			                                    .getSongName());
        return longest;
    }

    @Override
    public Optional<String> longestAlbum() {
    	double max = 0.0;
    	double val = 0.0;
    	String ris = null;

    	for (String a : albums.keySet()) {
    		val = songs.stream()
    				   .filter(s -> s.getAlbumName().equals(Optional.of(a)))
    				   .mapToDouble(s -> s.getDuration())
    				   .sum();	
    		if (val >= max) {
    			max = val;
    			ris = a;
    		}
    	}

        return Optional.of(ris);
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
